<center><span style="text-decoration:underline">**“Sending a gift from the app menu” Scenario**</span></center>

Implementation plan:

1. Adding an icon with a gift to the interface of your app's menu;
2. Adding the YouGiver Service presentation screens (the design is attached);
3. Adding a page with options for selecting gift sending scenarios; 
4. Setting the notifications sending for the users in the capacity of the Giver and the Recipient;
5. Invoking system and installed applications on a smartphone to send a link to another application; 
6. The implementation of the links to the YouGiver service (including to the gifts catalog), which open in the webView of your application.

<center>**Step-by-Step Description of Integration**</center>

**Step 1**  
The YouGiver icon is implemented in the menu interface of your application:
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_2_1.png" alt="title text"/>
</p>
When you click on the icon, a mini-presentation opens: 
<p align="center">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_2_2.png" alt="title text"/>
</p>

(the public source of the presentation screens is available [here](https://drive.google.com/drive/u/1/folders/15gTXMZAn3kCA2tVx4_jiObZhq2clpcuD))

Tapping on the "Learn more" link takes you to the YouGiver service website (URL: [https://www.yougiver.me/](https://www.yougiver.me/))  
Scenario screens (the public source of the scenario screens is available here). Tapping on the “Give a gift” button, the user is transferred to the page with a choice of gift sending options:
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_2_3.png" alt="title text"/>
</p>

**Step 2**  
When you tap on one of the available scenarios: “Gifts catalog”, “Send  via address”, “Choose a Presentee in any app”, the scenario is initialized, and your application sends the POST request to the YouGiver Service to [https://service.yougiver.me/api/v1/gift_requests](https://service.yougiver.me/api/v1/gift_requests) with the following parameters:

<!--table-->

| Parameter                              	| Discription                                	|
|---------------------------------------	|------------------------------------------	|
| gift_request[developer_number] **\*** 	| Your developer_number in YouGiver service  	|
| gift_request[giver_id] **\***         	| The Giver ID in your service       	|
| gift_request[giver_name]              	| Name of the Giver                             	|
| gift_request[giver_email]             	| Email of the Giver                           	|
| gift_request[giver_phone]             	| Giver's phone number                         	|
| gift_request[giver_nickname]          	| Nickname of the Giver                         	|
| gift_request[giver_gender]            	| Gender of the Giver                             

<!--endtable-->

<span style="color:red"> \* required parameter.</span>

The YouGiver Service sends the JSON reply to your app:

<!--table-->

| Paremeter | Description                                     |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->

(Visualization of the notification: the user in the capacity of the Giver receives a notification with the transfer button to the YouGiver catalog in the webView, where they select the gift and pay for it)
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_1_2.png" alt="title text"/>
</p>

**Step 3**  
**Testing.** In the Developer's Admin panel, in the Webhooks section, you can find the entire list of notifications you need to connect, with the field for entering URL.
 
**Event notifications for the “Send via address” Scenario, selected by the Giver**  
*(Description: A user in the capacity of the Giver knows all the contact details necessary to deliver the gift to the Recipient, who they independently fill in the YouGiver Service. In this case, the Giver receives a notification upon the successful/unsuccessful gift delivery to the chat of your app)*

1. **Event:** Change of order status to "Completed" → **№3 – Notifying the Giver that the gift was delivered to the Recipient.**
2. **Event:** Change of order status to "Not completed" → **№4 – Notifying the Giver that the gift was not delivered because of the Recipient.**

**Event notifications for the “Choose a Presentee in any app” Scenario, selected by the Giver**  
*(The user in the capacity of the Giver does not know the Recipient's contact information. The user who acts as the Recipient is NOT the user of your application. When this scenario is invoked, all system and installed applications are pulled up on the Giver's smartphone and it sends a link with the YouGiver form to one of the applications. The user in the capacity of the Giver receives notifications on the statuses of the order processing in the chat of your application.)*

1. **Event:** Change of order status to - "Completed" → **№3 – Notifying the Giver that the gift was delivered to the Recipient.**
2. **Event:** Change of order status to "Not completed" → **№4 – Notifying the Giver that the gift was not delivered because of the Recipient.**
3. **Event:** Provision of data for delivery by the Recipient → **№6 – Notifying the Giver that the Recipient has agreed to receive the gift.**
4. **Event:** 3b Scenario initialization → **№8 – Notification for the Giver with a link to be shared with Recipient.**

**Event notifications for the “Gifts catalog” Scenario, selected by the Giver**  
*(The user in the capacity of the Giver knows the country and city of the Recipient and can immediately go to the catalog to select and purchase a gift and independently send the link in a convenient way for them. The Giver receives notifications about the statuses of order processing in the chat of your application.)*

1. **Event:** Filling out contact information by the Presentee → **№2 – Notifying the Giver that Recipient has filled out the gift delivery information.**
2. **Event:** Change of order status to - "Completed" → **№3 – Notifying the Giver that the gift was delivered to the Recipient.**
3. **Event:** Change of order status to "Not completed" → **№4 – Notifying the Giver that the gift was not delivered because of the Recipient.**
4. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **№5 – Notifying the Giver that Recipient didn’t confirm (ignored) accepting of the gift.**
5. **Event:** Payment for the gift by the Giver → **№8 – Notification for the Giver with a link to be shared with Recipient.**

**Step 4**  
If you successfully passed the notification settings testing, please read the **Security Key Installation** section.