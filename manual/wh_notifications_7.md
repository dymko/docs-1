## Notification №7 "Notifying the Recipient. ”Why was this link sent to me."

**Example of the data structure in JSON that may be sent to the WebHook URL of the developer**

### Notification body
```json
  {
    "create_time": "2000-01-01 00:00:00 UTC",
    "event_type": "first_order",
    "resource_type": "notification",
    "resource": {
      "for": "uniq_recipient_id",
      "title": "short description",
      "message": "full description:\\nMake and receive real gifts with YouGiver!"
    }
  }
```
