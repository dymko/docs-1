**Integration Plan**

Consists of:

1. Choosing a YouGiver function placement scenario.
2. Adding screens/icons to the interface of your application (the design is attached).
3. Notifications setting. In the developer's account, create/set up WebHooks with the necessary event notifications according to the scenarios that you plan to integrate into your application.
4. Perform the initialization of the selected scenarios (using developer_number). 
5. The implementation of the links to the YouGiver service (including to the gifts catalog), which open in the webView of your application.

Select any of the scenarios that fit the concept of your application:

 - **"Sending a gift in a chat" Scenario** – the ability to send a gift directly from the chat (an icon is added to the chat interface)
 - **“Sending a gift from the app menu” Scenario** – the ability to send a gift from the app menu (an icon is added to the interface of the app menu and a screen-presentation of the YouGiver service/design is attached in the ready-made form)
- **“Sending a gift when viewing a profile” Scenario** – users will be able to send gifts while viewing the profile photo (a YouGiver button is added to the interface of the profile photo)
- **“Sending a gift in a few clicks. Form with delivery data for receiving gifts in your application” Scenario** - as a result of implementation, your users will be able to give gifts to each other in a few clicks, thereby significantly increase your income! 
When the form is filled, sending in a few clicks will be implemented when you invoke any scenario, the user in the capacity of the Giver will be immediately able to go to the catalog - select, pay for and send a gift.
