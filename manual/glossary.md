**Developer’s app** - the app that has been integrated / is being integrated with YouGiver.

**Giver** - a participant and user of developer’s app that performs the gift giving from the YouGiver catalog through one of the interaction scenarios.

**Recipient** - the process participant who receives the gift from the Giver.

**Gifts catalog** - catalog of available gifts in the marketplace. Available categories according to the Recipient's location: Bouquets; Cosmetics and perfumery; Electronics; Bijouterie; Stuffed Toys; Certificates of impressions (opens in the webView of your application).

**Scenario** - a business process, according to which the Giver can perform the gift giving to the Recipient from the YouGiver catalog.

**Notification** - a message that YouGiver sends to developer’s app to transfer it further to the Giver or Recipient. It is used for the completeness of the gift giving process and the informing of the participating parties.

**Design of integrable pages and elements in the developer's application** - the existing design of icons, buttons, and pages is attached, which identifies that the YouGiver gift service is integrated into the developer’s application (the design can be adapted for the brand book of the developer's application).