## Notification №8 "Notification for the Giver with a link to be shared with Recipient"

**Example of the data structure in JSON that may be sent to the WebHook URL of the developer**

### Notification body
```json
  {
    "create_time": "2000-01-01 00:00:00 UTC",
    "event_type": "gift_link_remember",
    "resource_type": "notification",
    "resource": {
      "for": "uniq_recipient_id",
      "title": "Your order #%{number} is successfully issued",
      "message": "Your order #%{number} is successfully issued for %{recipient_name}…"
    }
  }
```
