## Notification №4 "Notifying the Giver that the gift was not delivered because of the Recipient"

**Example of the data structure in JSON that may be sent to developer's WebHook URL**

### Notification body
```json
  {
    "create_time": "2000-01-01 00:00:00 UTC",
    "event_type": "order_canceled",
    "resource_type": "notification",
    "resource": {
      "for": "uniq_giver_id",
      "title": "short description",
      "message": "full description:\\nMake and receive real gifts with YouGiver!",
      "giver_name": "giver name",
      "recipient_name": "recipient name",
      "gift_request": "123456789",
      "links": {
        "order": "http://store.service.yougiver.me/order/12345678"
      }
    }
  }
```
