**YouGiver provides the ability to send any Notifications to both Developerís app (via WebHooks) and to E-mail of the Giver/Recipient.**

**Available options:**

1. If the Developer has connected WebHook with the Notification #X - upon the occurrence of the corresponding event, YouGiver will send the required Notification to the Developer side (to the URL specified in the WebHook).

2. If the Developer has connected WebHook with the Notification #X **AND** additionally sent the **E-mail** of the Giver/Recipient to YouGiver - upon the occurrence of the corresponding event YouGiver will send the required Notification to the Developer side (to the URL specified in the WebHook URL) and to the **E-mail** of the Giver/Recipient (depending on who is intended for the Notification). In this case, there will be duplication of the Notification #X on several delivery channels to the addressee.

3. If the Developer **hasn't** connected the WebHook with the Notification #X, but sent the E-mail of the Giver/Recipient to YouGiver - upon the occurrence of the corresponding event, YouGiver will send the required Notification **only** to the E-mail of the Giver/Recipient (depending on who the Notification is for).

4. If the Developer **hasn't** connected WebHook with the Notification #X and **hasn't** sent the E-mail of the Giver/Recipient to YouGiver's side - upon the occurrence of the relevant event, **the Notification #X will not be sent at all**.