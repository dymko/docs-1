<center><span style="text-decoration:underline">**"Sending a gift in a chat" Scenario**</span></center>

Implementation plan:
  
1. Adding an icon with a gift to the interface between a two-user chat; 
2. Setting the notifications sending for the users in the capacity of the Giver and the Recipient; 
3. The implementation of the links to the YouGiver service (including to the gifts catalog), which open in the webView of your application.

<center>**Step-by-Step Description of Integration**</center>

**Step 1**  
A gift icon is introduced to the chat interface to invoke the YouGiver Service scenario (the source of the public icons is [here](https://drive.google.com/drive/u/1/folders/1WezdLXl8gDG0L7d-4QkNXBOYQBqVIz4H?ogsrc=32)).
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_1_1.png" alt="title text"/>
</p>

**Step 2**  
When you tap on the gift icon in the chat, the scenario is initialized, and your application sends the POST request to the YouGiver Service to [https://service.yougiver.me/api/v1/gift_requests](https://service.yougiver.me/api/v1/gift_requests) with the following parameters:

<!--table-->

| Parameter                              	| Discription                                	|
|---------------------------------------	|------------------------------------------	|
| gift_request[developer_number] **\*** 	| Your developer_number in YouGiver service  	|
| gift_request[giver_id] **\***         	| The Giver ID in your service       	|
| gift_request[giver_nickname]          	| Nickname of the Giver                         	|
| gift_request[recipient_id] **\***      	| The Recipient ID in your service   	|
| gift_request[recipient_nickname]      	| Recipient's Nickname                       	|

<!--endtable-->

<span style="color:red"> \* required parameter.</span>

The YouGiver Service sends the JSON reply to your app:

<!--table-->

| Paremeter | Description                                     |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->

(Visualization of the notification: the user in the capacity of the Giver receives a notification with the transfer button to the YouGiver catalog in the webView, where they select the gift and pay for it)
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_1_2.png" alt="title text"/>
</p>

**Step 3**  
**Testing.** In the Webhooks section, you can find the entire list of notifications you need to connect, with the field for entering URL.

**Supported event notifications for the Scenario where the Giver knows the Recipient's country and city**  
*(Scenario Description: A user who acts as a Giver knows the country and city of the user who will act as the Recipient.)*

1. **Event:** Payment for the gift by the Giver → **№1 – Notifying the Recipient of a new gift.**
2. **Event:** Filling out contact information by the Recipient → **№2 – Notifying the Giver that Recipient has filled out the gift delivery information.**
3. **Event:** Change of order status to "Completed" → **№3 – Notifying the Giver that the gift was delivered to the Recipient.**
4. **Event:** Change of order status to "Not completed" → **№4 – Notifying the Giver that the gift was not delivered because of the Recipient.**
5. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **№5 – Notifying the Giver that Recipient didn’t confirm (ignored) accepting of the gift.**
6. **Event:** Payment by the gift Giver. In case the Recipient has never previously confirmed the receipt of the gift. → **№7 – Notifying the Recipient: Why was this link sent to me?**

**Supported event notifications for the Scenario where the Giver does not know the Recipient's country and city**  
*(Scenario Description: A user who acts as a Giver does NOT know the recipient's country and the city of residence. In this case, the form is sent as a link to the Recipient in the chat, from which the gift sending scenario was initialized, to fill the delivery data.)*

1. **Event:** Payment for the gift by the Giver → **№1 – Notifying the Recipient of a new gift.**
2. **Event:** Change of order status to "Completed" → **№3 – Notifying the Giver that the gift was delivered to the Recipient.**
3. **Event:** Change of order status to "Not completed" → **№4 – Notifying the Giver that the gift was not delivered because of the Recipient.**
4. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **№5 – Notifying the Giver that Recipient didn’t confirm (ignored) accepting of the gift.**
5. **Event:** Recipient has provided the delivery information → **№6 – Notifying the Giver that the Recipient has agreed to receive the gift.**
6. **Event:** Payment by the gift Giver. In case the Recipient has never previously confirmed the receipt of the gift. → **№7 – Notifying the Recipient: Why was this link sent to me?**

*It is recommended to set the correct display of the link preview of a new gift for the Recipient (optional). The public source is available [here](https://drive.google.com/drive/u/1/folders/13YF68hc9JUFADVJLiFbC6GiaiuEwobYX).*
 
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_1_3.png" alt="title text"/>
</p>

**Step 4**  
If you successfully passed the notification settings testing, please read the **Security Key Installation** section.