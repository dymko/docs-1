Sending notifications for the Giver and Recipient to Developerís app is provided through the connection of WebHooks in Developer's personal account.

**Creating** a Web Hook is possible in the "Webhooks" tab on the left sidebar in Developerís personal account:
<p align="center">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/webhooks_list.png" alt="title text"/>
</p>

When creating, you need:

- to specify the URL end-point on the part of the Developerís app, to which YouGiver should send notifications (1 WH contains 1 URL)
- to select one or more event notifications from the list that must be sent to the previously specified end-point

<p align="center">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/webhook_add.png" alt="title text"/>
</p>  
  
If there is a need to receive different Notifications to different URLs, you need to create several WebHooks with different URLs and connect the appropriate Notifications to each WebHook.

On the "Webhooks" tab, you can also **view** and **manage** the list of existing Web Hooks.