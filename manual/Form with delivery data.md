<center><span style="text-decoration:underline">**“Sending a gift in a few clicks. Form with delivery data for receiving gifts in your application” Scenario**</span></center>

**As a result of implementation, your users will be able to give gifts to each other in a few clicks, thereby significantly increase your income! 
Sending in a few clicks will be implemented when you invoke any scenario, the user in the capacity of the Giver will be immediately able to go to the catalog - select, pay for and send a gift.**

Implementation plan:

1. Implementing an additional feature in User profile settings of your app (Recommended name “YouGiver. Gift details”).
2. Adding a screen with a form to enter delivery information.
3. The implementation of the links to the YouGiver service (including to the gifts catalog), which open in the webView of your application.
4. Adding a list of served cities by the YouGiver service.
5. Setting the notifications sending for the user in the capacity of the Giver and the Recipient.

<center>**Step-by-Step Description of Integration**</center>

**Step 1**  
The “YouGiver. Gift details” button is implemented in the User Profile interface of your app (the public source is available here).
 
(example)
<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_4_1.png" alt="title text"/>
</p>

**Step 2**  
When you tap “YouGiver. Gift details” button, the form for filling the delivery data opens (the public source of the form with the delivery data is available [here](https://drive.google.com/drive/u/1/folders/1Ghe93WgP8FpTRe_C-lcvn4MwLOTlzkVP)).

<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_4_2.png" alt="title text"/>
</p>

When you tap the “Learn more” link, you are transferred to the YouGiver service website (URL: [https://www.yougiver.me/](https://www.yougiver.me/))
 
Adding a list of served cities by the YouGiver service
 
If there is no user city on the list, they can apply to add a city by tapping on the link “Did not find your city?” (URL: [https://www.yougiver.me/delivery](https://www.yougiver.me/delivery))

**Step 3**  
**When you tap the** YouGiver icon/button (you can invoke any already implemented scenario: from the chat; from the menu; when viewing the Profile)
 
Your application sends the POST request to the YouGiver Service to [https://service.yougiver.me/api/v1/gift_requests](https://service.yougiver.me/api/v1/gift_requests) with the following parameters:

<!--table-->

| Parameter                              	| Discription                                	|
|---------------------------------------	|------------------------------------------	|
| gift_request[developer_number] **\*** 	| Your developer_number in YouGiver service  	|
| gift_request[giver_id] **\***         	| The Giver ID in your service       	|
| gift_request[giver_name]              	| Name of the Giver                             	|
| gift_request[giver_email]             	| Email of the Giver                           	|
| gift_request[giver_phone]             	| Giver's phone number                         	|
| gift_request[giver_nickname]          	| Nickname of the Giver                         	|
| gift_request[giver_gender]            	| Gender of the Giver                             	|
| gift_request[recipient_id] **\***      	| The Recipient ID in your service   	|
| gift_request[recipient_name]          	| Recipient's name                           	|
| gift_request[recipient_email]         	| Recipient's email                         	|
| gift_request[recipient_country] **\*** 	| Country of the Recipient                        	|
| gift_request[recipient_city] **\***    	| City of the Recipient                         	|
| gift_request[recipient_address]       	| Presentee's address                         	|
| gift_request[recipient_gender]        	| Gender of the Recipient                           	|
| gift_request[recipient_nickname]      	| Recipient's Nickname                       	|

<!--endtable-->

<span style="color:red"> \* required parameter.</span>

**Paremeter Description**

1. All parameters are simple strings:  
  - <span style="color:red">gift_request[recipient_country]</span> — The country must be serviced by YouGiver.
  - <span style="color:red">gift_request[recipient_city]</span> — The city must be serviced by YouGiver.
2. For a list of the countries and cities YouGiver supports, see the "[Locations API](https://yougiver.readthedocs.io/en/latest/locations_api/)" section.

<span style="color:red"> \* required parameter.</span>

The YouGiver Service sends the JSON reply to your app:

<!--table-->

| Paremeter | Description                                     |
|--------- |---------------------------------------------- |
| url      | Link the Giver must follow |

<!--endtable-->

**Step 4**  
The users of your application, acting as the Giver and the Recipient, receive notifications from the YouGiver service in the chat, depending on the scenario.

<p align="left">
  <img src="https://bitbucket.org/tiwinnovations/docs/raw/master/images/Step_1_2.png" alt="title text"/>
</p>

**Supported event notifications when the User acting as the Recipient filled the form for receiving gifts**  
*(Description: The YouGiver service knows the Recipient's contact information necessary for the gift delivery, allowing the user of your application, acting as the Giver, to process a gift "in a few clicks".)*

1. **Event:** Payment for the gift by the Giver → **№1 – Notifying the Recipient of a new gift.**
2. **Event:** Filling out contact information by the Presentee → **№2 – Notifying the Giver that Recipient has filled out the gift delivery information.**
3. **Event:** Change of order status to "Completed" → **№3 – Notifying the Giver that the gift was delivered to the Recipient.**
4. **Event:** Change of order status to "Not completed" → **№4 – Notifying the Giver that the gift was not delivered because of the Recipient.**
5. **Event:** Expiration of X hours allocated for gift confirmation. (At the moment X = 48 hours) → **№5 – Notifying the Giver that Recipient didn’t confirm (ignored) accepting of the gift.**
6. **Event:** Payment by the gift Giver. In case the Presentee has never previously confirmed the receipt of the gift. → **№6 – Notifying the Giver that the Recipient has agreed to receive the gift.**

**Step 5**  
If you successfully passed the notification settings testing, please read the **Security Key Installation section**.